#!/bin/bash

set -e

export INSTALL="$(pwd)/install"
export DOWNLOADS="$(pwd)/downloads"
export DEB_VERSION=12.7771

mkdir -p $INSTALL
mkdir -p $DOWNLOADS
rm -rf $INSTALL/*

[ -f $DOWNLOADS/spm12-$DEB_VERSION.zip ] || wget -O  $DOWNLOADS/spm12-$DEB_VERSION.zip http://www.fil.ion.ucl.ac.uk/spm/download/restricted/eldorado/spm12.zip
cd $INSTALL
mkdir -p opt
cd opt
unzip $DOWNLOADS/spm12-$DEB_VERSION.zip
# Check the version of SPM
grep 7771 spm12/Contents.m

cd $INSTALL
mkdir -p etc/profile.d/
printf "\n# SPM\nexport SPM_HOME=/opt/spm12\n" >> etc/profile.d/spm12.sh

mkdir -p usr/share/applications
cat <<EOF > usr/share/applications/SPM12.desktop
[Desktop Entry]
Type=Application
Name=SPM 12
Icon=/opt/spm12/help/images/spm12.png
Exec=env SPM_ROOT=/opt/spm12 /opt/spm12/bin/spm12-matlab
Terminal=false
Categories=Utility;Science;MedicalSoftware;
EOF

cd ..

fpm -s dir -t deb -n spm -v $DEB_VERSION \
   --description="SPM 12" \
  --before-remove before-remove.sh \
  --deb-suggests "mcr = 0.2018b" \
  -C $INSTALL opt/ etc/ usr/
